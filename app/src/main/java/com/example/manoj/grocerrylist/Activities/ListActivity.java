package com.example.manoj.grocerrylist.Activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.manoj.grocerrylist.Data.DataBaseHandler;
import com.example.manoj.grocerrylist.Model.Grocery;
import com.example.manoj.grocerrylist.R;
import com.example.manoj.grocerrylist.UI.RecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerViewAdapter recyclerViewAdapter;
    private List<Grocery>groceryList;
    private List<Grocery>listItems;
    private DataBaseHandler db;


    private AlertDialog.Builder dialogbuilder;
    private AlertDialog dialog;
    private EditText grocerryitem;
    private EditText grocerryquantity;
    private Button saveButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab1);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                   //     .setAction("Action", null).show();
                createPopupDialog();
            }
        });

        db = new DataBaseHandler(this);
        recyclerView = (RecyclerView)findViewById(R.id.recyclerViewID);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        groceryList = new ArrayList<>();
        listItems = new ArrayList<>();
        // get items from from the database;
        groceryList = db.getAllGrocery();

    for(Grocery c: groceryList){
        Grocery grocery = new Grocery();
        grocery.setName(c.getName());
        grocery.setQuantity( " Qty:  "+c.getQuantity());
        grocery.setId(c.getId());
        grocery.setDateItemAdded("Added on :"+c.getDateItemAdded());
        listItems.add(grocery);
    }

        recyclerViewAdapter = new RecyclerViewAdapter(this ,listItems);
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerViewAdapter.notifyDataSetChanged();


    }



    public void createPopupDialog(){
        dialogbuilder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.popup,null);
        grocerryitem = (EditText) view.findViewById(R.id.grocerry);
        grocerryquantity =(EditText) view.findViewById(R.id.quantity);
        saveButton = (Button) view.findViewById(R.id.save);
        dialogbuilder.setView(view);
        dialog = dialogbuilder.create();
        dialog.show();
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Todo: save to the database
                // Todo: go to next screen
                if(!grocerryitem.getText().toString().isEmpty()
                        &&!grocerryquantity.getText().toString().isEmpty()) {
                    saveGrocerryToDB(v);
                }
            }
        });
    }



    private void saveGrocerryToDB(View v) {
        Grocery grocery = new Grocery();
        String newGrocerry = grocerryitem.getText().toString();
        String newGroceryQuantity = grocerryquantity.getText().toString();
        grocery.setName(newGrocerry);
        grocery.setQuantity(newGroceryQuantity);

        // save to database;
        db.addGrocerry(grocery);
        Snackbar.make(v,"Item Saved ! " , Snackbar.LENGTH_LONG).show();
        Log.d("Item Added ID : ",String.valueOf(db.getGroceryCount()));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                dialog.dismiss();
                recreate();
            }
        },1000);
    }

}
