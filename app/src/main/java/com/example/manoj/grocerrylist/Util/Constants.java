package com.example.manoj.grocerrylist.Util;

/**
 * Created by MANOJ on 12/28/2017.
 */

public class Constants {
    public static final int DB_VERSION = 1;
    public static final String DB_NAME ="grocerryListDB";
    public static final String TABLE_NAME ="groceryTBL";
    // TABLE COLUMN
    public static final String KEY_ID ="id";
    public static final String KEY_GROCERY_ITEM ="grocerry_item";
    public static final String KEY_QTY_NUMBER ="quantity_number";
    public static final String KEY_DATE_NAME ="date_added";


}
