package com.example.manoj.grocerrylist.UI;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.manoj.grocerrylist.Activities.Details;
import com.example.manoj.grocerrylist.Data.DataBaseHandler;
import com.example.manoj.grocerrylist.Model.Grocery;
import com.example.manoj.grocerrylist.R;

import java.util.List;

import static com.example.manoj.grocerrylist.R.id.grocerry;

/**
 * Created by MANOJ on 12/29/2017.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{
    private Context context;
    private List<Grocery> groceryItems;
    private AlertDialog.Builder alertDialogBuilder;
    private AlertDialog dialog;
    private LayoutInflater inflater;


    public RecyclerViewAdapter(Context context, List<Grocery> groceryItems) {
        this.context = context;
        this.groceryItems = groceryItems;
    }



    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row,parent,false);


        return new ViewHolder(view,context);
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.ViewHolder holder, int position) {
      Grocery grocery = groceryItems.get(position);
        holder.groceryItemName.setText(grocery.getName());
        holder.Quantity.setText(grocery.getQuantity());
        holder.dateAdded.setText(grocery.getDateItemAdded());
    }

    @Override
    public int getItemCount() {
        return groceryItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView groceryItemName;
        public TextView Quantity;
        public TextView dateAdded;
        public Button editButton;
        public Button deleteButton;
        public FloatingActionButton fabButton;
        public int id;
        public ViewHolder(View itemView, Context ctx) {
            super(itemView);
            context = ctx ;
            groceryItemName = (TextView)itemView.findViewById(R.id.name);
            Quantity = (TextView)itemView.findViewById(R.id.Qty);
            dateAdded =(TextView)itemView.findViewById(R.id.dateAdded);
            editButton =(Button)itemView.findViewById(R.id.editButton);
            deleteButton =(Button)itemView.findViewById(R.id.deleteButton);
            //fabButton = (FloatingActionButton)itemView.findViewById(R.id.fab);

            editButton.setOnClickListener(this);
            deleteButton.setOnClickListener(this);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // go to newxt screen/detail Activity
                    int position = getAdapterPosition();
                    Grocery grocery = groceryItems.get(position);
                    Intent intent = new Intent(context, Details.class);
                    intent.putExtra("name",grocery.getName());
                    intent.putExtra("quantity",grocery.getQuantity());
                    intent.putExtra("id",grocery.getId());
                    intent.putExtra("date",grocery.getDateItemAdded());
                    context.startActivity(intent);
                }
            });
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.editButton:
                    int position = getAdapterPosition();
                    Grocery grocery = groceryItems.get(position);
                    editItem(grocery);
                    break;
                case R.id.deleteButton:
                     position = getAdapterPosition();
                     grocery = groceryItems.get(position);
                    deleteItem(grocery.getId());

                    break;
                case R.id.fab:
                    Grocery groceryl = new Grocery();
                    addGrocery2(groceryl);
                    break;
            }

        }
        public void deleteItem(final int id){

            alertDialogBuilder = new AlertDialog.Builder(context);
            inflater = LayoutInflater.from(context);
            View view = inflater.inflate(R.layout.conformation_dialog,null);
            Button noButton = (Button)view.findViewById(R.id.noButton);
            Button yesButton = (Button)view.findViewById(R.id.yesButton);
            alertDialogBuilder.setView(view);
            dialog = alertDialogBuilder.create();
            dialog.show();
            noButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            yesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // delete the item
                    DataBaseHandler db = new DataBaseHandler(context);
                    db.deleteGrocery(id);
                    groceryItems.remove(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());
                    dialog.dismiss();
                }
            });


        }

        public void editItem(final Grocery grocery){

            alertDialogBuilder = new AlertDialog.Builder(context);
            inflater = LayoutInflater.from(context);
            final View view = inflater.inflate(R.layout.popup,null);
            final EditText groceryItem = (EditText) view.findViewById(grocerry);
            final EditText quantity =(EditText)view.findViewById(R.id.quantity);
            final TextView title = (TextView)view.findViewById(R.id.tittle);
            title.setText("Edit Grocery ");
            Button saveButton = (Button)view.findViewById(R.id.save);
            alertDialogBuilder.setView(view);
            dialog = alertDialogBuilder.create();
            dialog.show();

            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DataBaseHandler db = new DataBaseHandler(context);
                    // update item
                    grocery.setName(groceryItem.getText().toString());
                    grocery.setQuantity(quantity.getText().toString());
                    if(!groceryItem.getText().toString().isEmpty()
                            && !quantity.getText().toString().isEmpty()){
                        db.updateGrocery(grocery);
                        notifyItemChanged(getAdapterPosition(),grocery);

                    }else{
                        Snackbar.make(view, "Add Grocery and quantity ",Snackbar.LENGTH_LONG).show();
                    }
                     dialog.dismiss();
                }
            });
        }




        /////

        public void addGrocery2(final Grocery grocery){

            alertDialogBuilder = new AlertDialog.Builder(context);
            inflater = LayoutInflater.from(context);
            final View view = inflater.inflate(R.layout.popup,null);
            final EditText groceryItem = (EditText) view.findViewById(grocerry);
            final EditText quantity =(EditText)view.findViewById(R.id.quantity);
            final TextView title = (TextView)view.findViewById(R.id.tittle);
            title.setText("Edit Grocery ");
            Button saveButton = (Button)view.findViewById(R.id.save);
            alertDialogBuilder.setView(view);
            dialog = alertDialogBuilder.create();
            dialog.show();

            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DataBaseHandler db = new DataBaseHandler(context);
                    // update item
                    grocery.setName(groceryItem.getText().toString());
                    grocery.setQuantity(quantity.getText().toString());
                    if(!groceryItem.getText().toString().isEmpty()
                            && !quantity.getText().toString().isEmpty()){
                        db.addGrocerry(grocery);
                        notifyItemChanged(getAdapterPosition(),grocery);

                    }else{
                        Snackbar.make(view, "Add Grocery and quantity ",Snackbar.LENGTH_LONG).show();
                    }
                    dialog.dismiss();
                }
            });
        }
        /////

    }
}
