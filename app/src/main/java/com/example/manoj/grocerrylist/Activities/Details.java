package com.example.manoj.grocerrylist.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.manoj.grocerrylist.R;

public class Details extends AppCompatActivity {
    private TextView itemName;
    private TextView quantity;
    private TextView dateAdded;
    private int groceryID;




    private Button gotoSecond;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        itemName = (TextView)findViewById(R.id.itemNameDet);
        quantity =(TextView)findViewById(R.id.quantityDet);
        dateAdded =(TextView)findViewById(R.id.dateAddedDet);
        Bundle bundle = getIntent().getExtras();




        gotoSecond = (Button) findViewById(R.id.button);
        gotoSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Details.this, ChatActivity2.class);
                startActivity(intent);
            }
        });





        if(bundle != null){
            itemName.setText(bundle.getString("name"));
            quantity.setText(bundle.getString("quantity"));
            dateAdded.setText(bundle.getString("date"));
            groceryID = bundle.getInt("id");
        }
    }
}
