package com.example.manoj.grocerrylist.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.manoj.grocerrylist.Data.DataBaseHandler;
import com.example.manoj.grocerrylist.Model.Grocery;
import com.example.manoj.grocerrylist.R;

public class MainActivity extends AppCompatActivity {
    private AlertDialog.Builder dialogbuilder;
    private AlertDialog dialog;
    private EditText grocerryitem;
    private EditText grocerryquantity;
    private Button saveButton;
    private DataBaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new DataBaseHandler(this);
        byPassActivity();
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                 createPopupDialog();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void createPopupDialog(){
        dialogbuilder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.popup,null);
        grocerryitem = (EditText) view.findViewById(R.id.grocerry);
        grocerryquantity =(EditText) view.findViewById(R.id.quantity);
        saveButton = (Button) view.findViewById(R.id.save);
        dialogbuilder.setView(view);
        dialog = dialogbuilder.create();
        dialog.show();
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Todo: save to the database
                // Todo: go to next screen
                if(!grocerryitem.getText().toString().isEmpty()
                        &&!grocerryquantity.getText().toString().isEmpty()) {
                    saveGrocerryToDB(v);
                }
            }
        });
    }

    private void saveGrocerryToDB(View v) {
        Grocery grocery = new Grocery();
        String newGrocerry = grocerryitem.getText().toString();
        String newGroceryQuantity = grocerryquantity.getText().toString();
        grocery.setName(newGrocerry);
        grocery.setQuantity(newGroceryQuantity);

        // save to database;
        db.addGrocerry(grocery);
        Snackbar.make(v,"Item Saved ! " , Snackbar.LENGTH_LONG).show();
        Log.d("Item Added ID : ",String.valueOf(db.getGroceryCount()));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                dialog.dismiss();
                startActivity(new Intent(MainActivity.this, ListActivity.class));
            }
        },1000);
    }
    public void byPassActivity(){
        // checks if database is empty if not the we must
        // go to the ListActivity and show all the data
        if(db.getGroceryCount() >0){
            startActivity(new Intent(MainActivity.this,ListActivity.class));
            finish();
        }
    }
}
